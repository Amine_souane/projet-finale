import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactService } from '../contact.service';
import { Contact } from '../entities';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-contact-admin',
  templateUrl: './contact-admin.component.html',
  styleUrls: ['./contact-admin.component.css']
})
export class ContactAdminComponent implements OnInit {
  contacts:Contact[] = []

  contact:Contact = {
    name:'',
    email:'',
    phoneNumber:'',
    message:''
  }

  constructor(private contactService:ContactService, private route:Router) { }

  deleteMessage(contact:Contact){
    console.log
    this.contactService.delete(contact.id!).subscribe();
    this.updateContactList(contact)
     }
     updateContactList(contact:Contact){
       this.contacts.splice(this.contacts.findIndex(item => item.id==contact.id),1)
     }

  ngOnInit(): void {
    
    this.contactService.getAll().subscribe(data => this.contacts = data);
  }
  

}
