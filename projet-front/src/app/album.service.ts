import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Album } from './entities';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Album[]>('http://localhost:8080/api/album');
  }

  getById(id:number) {
    return this.http.get<Album>('http://localhost:8080/api/album/'+id)
  }

  add(album:Album, upload:File) {
    const form = new FormData();
    form.append('name', album.name);
    form.append('upload', upload);
    return this.http.post<Album>(environment.apiUrl+'/api/album/', form);
  } 


  delete(id:number) {
    return this.http.delete(environment.apiUrl+'/api/album/'+id);
  }

  put(album:Album){
    return this.http.put<Album>(environment.apiUrl+'/api/album/'+album.id, album);
  } 


}

