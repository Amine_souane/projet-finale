import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { Image } from '../entities';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-add-image',
  templateUrl: './add-image.component.html',
  styleUrls: ['./add-image.component.css']
})
export class AddImageComponent implements OnInit {
  images:Image[] = []

  image: Image = {
    image:'',
    date:''
  };

  upload?: File;

  IdAlbum?:number

  constructor(private imageService: ImageService, private router: ActivatedRoute, private route: Router) { }

  loadImages(){
    this.imageService.getByIdAlbum(this.IdAlbum!).subscribe(data => this.images = data)
  }

  ngOnInit(): void {
    this.router.params.pipe(
      switchMap(params => (this.IdAlbum=params['id']))
    )
    .subscribe(()=>{this.loadImages();
    });
  }

  addImage() {

    this.imageService.add(this.image, this.upload!, this.IdAlbum!).subscribe((data) => {
      this.route.navigate(['/admin-page/'+this.IdAlbum])
    });
  }
  changeFile(event: any) {
    if (event?.target.files.length > 0) {
      this.upload= event?.target.files[0];
    }
  }
}
