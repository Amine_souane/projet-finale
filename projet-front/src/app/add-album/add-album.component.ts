import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlbumService } from '../album.service';
import { Album } from '../entities';

@Component({
  selector: 'app-add-album',
  templateUrl: './add-album.component.html',
  styleUrls: ['./add-album.component.css']
})
export class AddAlbumComponent implements OnInit {
  album: Album = {
    name: '',
    cover: ''

  };

  upload?: File;

  constructor(private albumService: AlbumService, private router: Router) { }

  ngOnInit(): void {
  }

  addAlbum() {

    this.albumService.add(this.album, this.upload!).subscribe((data) => {
      this.router.navigate(['/admin-page/'])
    });

  }
  changeFile(event: any) {
    if (event?.target.files.length > 0) {
      this.upload = event?.target.files[0];
    }
  }
}
