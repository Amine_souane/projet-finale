import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactService } from '../contact.service';
import { Contact } from '../entities';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {
  contact:Contact = {
    name:'',
    email:'',
    phoneNumber:'',
    message:''
  };

  constructor(private contactService:ContactService, private router:Router) { }

  ngOnInit(): void {

  }

  addContact() {
    this.contactService.send(this.contact).subscribe(() => {
      this.router.navigate(['/']);
    });
  }

}
