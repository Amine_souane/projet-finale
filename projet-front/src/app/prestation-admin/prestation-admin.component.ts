import { Component, OnInit } from '@angular/core';
import { Prestation } from '../entities';
import { PrestationService } from '../prestation.service';

@Component({
  selector: 'app-prestation-admin',
  templateUrl: './prestation-admin.component.html',
  styleUrls: ['./prestation-admin.component.css']
})
export class PrestationAdminComponent implements OnInit {
  prestations:Prestation[] = []

  prestation:Prestation = {
    title:'',
    description:'',
    pictureNumber:0,
    price:0
  }

  constructor(private prestationService:PrestationService) { }

  deletePrestation(prestation:Prestation){
    console.log
    this.prestationService.delete(prestation.id!).subscribe();
    this.updatePrestationList(prestation)
     }
     updatePrestationList(prestation:Prestation){
       this.prestations.splice(this.prestations.findIndex(item => item.id==prestation.id),1)
     }

  ngOnInit(): void {
    this.prestationService.getAll().subscribe(data => this.prestations = data);
  }

}
