import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrestationAdminComponent } from './prestation-admin.component';

describe('PrestationAdminComponent', () => {
  let component: PrestationAdminComponent;
  let fixture: ComponentFixture<PrestationAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrestationAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrestationAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
