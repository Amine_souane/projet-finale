import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Contact } from './entities';


@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Contact[]>('http://localhost:8080/api/contact')
  }

  send(contact:Contact){
    return this.http.post<Contact>(environment.apiUrl+'/api/contact/', contact);
  } 

  delete(id:number) {
    return this.http.delete(environment.apiUrl+'/api/contact/'+id);
  }
}
