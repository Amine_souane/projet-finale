export interface Admin{
    id:number;
    email:string;
    password:string;
}

export interface Album {
    id?:number;
    name:string;
    cover:string;
}

export interface Contact {
    id?:number;
    name:string;
    email:string;
    phoneNumber:string;
    message:string
}

export interface Image {
    id?:number;
    image:string;
    date?:string;
    
}

export interface Prestation {
    id?:number;
    title:string;
    description:string;
    pictureNumber:number;
    price:number
}