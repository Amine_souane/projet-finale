import { Component, OnInit } from '@angular/core';
import { Prestation } from '../entities';
import { PrestationService } from '../prestation.service';

@Component({
  selector: 'app-prestation',
  templateUrl: './prestation.component.html',
  styleUrls: ['./prestation.component.css']
})
export class PrestationComponent implements OnInit {
  prestation:Prestation[] = []

  constructor(private prestationService:PrestationService) { }

  ngOnInit(): void {
    this.prestationService.getAll().subscribe(data => this.prestation = data);
  }

}
