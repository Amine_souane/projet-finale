import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Image } from '../entities';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-image-admin',
  templateUrl: './image-admin.component.html',
  styleUrls: ['./image-admin.component.css']
})
export class ImageAdminComponent implements OnInit {
  images:Image[] = []

  image:Image = {
    image:'',
    date:''
  }  

  IdAlbum?:number

  constructor(private imageService:ImageService, private route:ActivatedRoute) { }

  deleteImage(image:Image){
    console.log
    this.imageService.delete(image.id!).subscribe();
    this.updateImageList(image)
     }
     updateImageList(image:Image){
       this.images.splice(this.images.findIndex(item => item.id==image.id),1)
     }

  loadImages(){
    this.imageService.getByIdAlbum(this.IdAlbum!).subscribe(data => this.images = data)
  }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => (this.IdAlbum=params['id']))
    )
    .subscribe(()=>{this.loadImages();
    });
  }
}
