import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Prestation } from '../entities';
import { PrestationService } from '../prestation.service';

@Component({
  selector: 'app-add-prestation',
  templateUrl: './add-prestation.component.html',
  styleUrls: ['./add-prestation.component.css']
})
export class AddPrestationComponent implements OnInit {

  prestation:Prestation = {
    title:'',
    description:'',
    pictureNumber:0,
    price:0
  }

  constructor(private prestationService:PrestationService, private router:Router) { }

  ngOnInit(): void {
  }

  addPrestation() {
    this.prestationService.add(this.prestation).subscribe(() => {
      this.router.navigate(['/admin-prestation'])
    })
  }
}
