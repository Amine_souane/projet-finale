import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../album.service';
import { Album } from '../entities';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  albums:Album[] = []

  constructor(private albumService:AlbumService) { }

  ngOnInit(): void {
    this.albumService.getAll().subscribe(data => this.albums = data);

  }

}
