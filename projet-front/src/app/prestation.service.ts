import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Prestation } from './entities';

@Injectable({
  providedIn: 'root'
})
export class PrestationService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Prestation[]>('http://localhost:8080/api/prestation');
  }

  put(prestation:Prestation){
    return this.http.put<Prestation>(environment.apiUrl+'/api/prestation/', prestation);
  } 

  delete(id:number) {
    return this.http.delete(environment.apiUrl+'/api/prestation/'+id);
  }

  getById(id:number) {
    return this.http.get<Prestation>(environment.apiUrl+'/api/prestation/'+id)
  }

  add(prestation:Prestation){
    return this.http.post<Prestation>(environment.apiUrl+'/api/prestation/', prestation);
  } 
}

