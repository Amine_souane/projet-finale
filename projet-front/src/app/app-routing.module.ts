import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddAlbumComponent } from './add-album/add-album.component';
import { AddImageComponent } from './add-image/add-image.component';
import { AddPrestationComponent } from './add-prestation/add-prestation.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { AdminComponent } from './admin/admin.component';
import { ContactAdminComponent } from './contact-admin/contact-admin.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { FootersComponent } from './footers/footers.component';
import { GalleryComponent } from './gallery/gallery.component';
import { HomeComponent } from './home/home.component';
import { ImageAdminComponent } from './image-admin/image-admin.component';
import { ImageOfAlbumComponent } from './image-of-album/image-of-album.component';
import { PrestationAdminComponent } from './prestation-admin/prestation-admin.component';
import { PrestationComponent } from './prestation/prestation.component';
import { UpdatePrestationComponent } from './update-prestation/update-prestation.component';

const routes: Routes = [
  {path: '',component: HomeComponent},
  {path: 'galerie',component: GalleryComponent},
  {path: 'contactez-moi',component: ContactFormComponent},
  {path: 'prestation',component: PrestationComponent},
  {path: 'footers' ,component: FootersComponent},
  {path: 'galerie/:id' ,component: ImageOfAlbumComponent},
  {path: 'admin' ,component: AdminComponent},
  {path: 'admin-page' ,component: AdminPageComponent},
  {path: 'admin-page/:id' ,component: ImageAdminComponent},
  {path: 'contact-admin' ,component: ContactAdminComponent},
  {path: 'add-album' ,component: AddAlbumComponent},
  {path: 'admin-prestation' ,component: PrestationAdminComponent},
  {path: 'update-prestation/:id' ,component: UpdatePrestationComponent},
  {path: 'add-prestation' ,component: AddPrestationComponent},
  {path: 'add-image/:id' ,component: AddImageComponent}

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
