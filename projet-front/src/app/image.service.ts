import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AlbumService } from './album.service';
import { Image } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Image[]>('http://localhost:8080/api/image');
  } 

getByIdAlbum(id:number) {
  return this.http.get<Image[]>('http://localhost:8080/api/image/album/'+id);
}

getById(id:number) {
  return this.http.get<Image>('http://localhost:8080/api/album/'+id)
}

add(image:Image, upload:File, id:number){
  const form = new FormData(); 
  form.append('upload', upload);
  return this.http.post<Image>(environment.apiUrl+'/api/image/'+id, form);
} 


delete(id:number) {
  return this.http.delete(environment.apiUrl+'/api/image/'+id);
}

put(image:Image){
  return this.http.put<Image>(environment.apiUrl+'/api/image/'+image.id, image);
} 

}



