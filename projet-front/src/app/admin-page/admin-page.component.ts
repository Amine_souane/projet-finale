import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../album.service';
import { Admin, Album } from '../entities';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {
  albums:Album[] = []

  album:Album = {
    name:'',
    cover:''
  };

  constructor(private albumService:AlbumService) { }

  deleteAlbum(album:Album){
    console.log
    this.albumService.delete(album.id!).subscribe();
    this.updateAlbumList(album)
     }
     updateAlbumList(album:Album){
       this.albums.splice(this.albums.findIndex(item => item.id==album.id),1)
     }

  ngOnInit(): void {
    this.albumService.getAll().subscribe(data => this.albums = data);

  }

  


}

