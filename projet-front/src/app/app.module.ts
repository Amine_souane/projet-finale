import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PrestationComponent } from './prestation/prestation.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { HeadersComponent } from './headers/headers.component';
import { FootersComponent } from './footers/footers.component';
import { ImageOfAlbumComponent } from './image-of-album/image-of-album.component';
import { AdminComponent } from './admin/admin.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { ImageAdminComponent } from './image-admin/image-admin.component';
import { ContactAdminComponent } from './contact-admin/contact-admin.component';
import { FormsModule } from '@angular/forms';
import { PrestationAdminComponent } from './prestation-admin/prestation-admin.component';
import { AddAlbumComponent } from './add-album/add-album.component';
import { UpdatePrestationComponent } from './update-prestation/update-prestation.component';
import { AddPrestationComponent } from './add-prestation/add-prestation.component';
import { AddImageComponent } from './add-image/add-image.component';
import { HeadersAdminComponent } from './headers-admin/headers-admin.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PrestationComponent,
    GalleryComponent,
    ContactFormComponent,
    HeadersComponent,
    FootersComponent,
    ImageOfAlbumComponent,
    AdminComponent,
    AdminPageComponent,
    ImageAdminComponent,
    ContactAdminComponent,
    PrestationAdminComponent,
    AddAlbumComponent,
    UpdatePrestationComponent,
    AddPrestationComponent,
    AddImageComponent,
    HeadersAdminComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
