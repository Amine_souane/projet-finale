import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Image } from '../entities';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-image-of-album',
  templateUrl: './image-of-album.component.html',
  styleUrls: ['./image-of-album.component.css']
})
export class ImageOfAlbumComponent implements OnInit {
  image:Image[] = []

  constructor(private imageService:ImageService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.imageService.getByIdAlbum(params['id']))
    )
    .subscribe(data => this.image = data);
  }

}
