import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageOfAlbumComponent } from './image-of-album.component';

describe('ImageOfAlbumComponent', () => {
  let component: ImageOfAlbumComponent;
  let fixture: ComponentFixture<ImageOfAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageOfAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageOfAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
