import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Prestation } from '../entities';
import { PrestationService } from '../prestation.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-update-prestation',
  templateUrl: './update-prestation.component.html',
  styleUrls: ['./update-prestation.component.css']
})
export class UpdatePrestationComponent implements OnInit {
  prestations: Prestation[] = []

  prestation?: Prestation;

constructor(private prestationService: PrestationService, private router: ActivatedRoute, private route: Router) { }

ngOnInit(): void {
  this.router.params.pipe(
    switchMap(params => this.prestationService.getById(params['id']))
    ).subscribe(data => this.prestation = data);
  

}
update(prestation: Prestation) {

  this.prestationService.put(prestation).subscribe();  
  this.route.navigate(['/admin-prestation']);

}
  

}
