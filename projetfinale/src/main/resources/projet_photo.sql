-- Active: 1651580151384@@127.0.0.1@3306@projet_photo
use projet_photo;
DROP DATABASE projet_photo;
CREATE DATABASE projet_photo;
CREATE TABLE album( 
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64),
    cover VARCHAR(128)
);
CREATE TABLE image(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    id_album INT UNSIGNED,
    FOREIGN KEY (id_album) REFERENCES album(id) ON DELETE CASCADE,
    image_name VARCHAR(128),
    date DATE
);
INSERT INTO album(name, cover) VALUES ("Photo Portrait", "portrait-cover.jpg"), 
("Mariage", "mariage-cover.jpg"),
("Famillle", "cover-famille.jpg");
SET FOREIGN_KEY_CHECKS=0;
INSERT INTO image(id_album, image_name, date) VALUES (1, "portrait-1.jpg", "2021-06-20"), 
(2, "mariagepaul.jpg", "2022-01-15"),
(3, "photosoeurjumelle.jpg", "2022-05-15"),
(3, "FamilleDubois.jpg", "2022-06-13"),
(1, "portraitenfant.jpg", "2022-02-05");


SELECT id,id_album,image,`date` FROM image;

ALTER TABLE image DROP image;

ALTER TABLE image ADD image_name VARCHAR(250);
CREATE TABLE prestation(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64),
    description VARCHAR(512),
    picturenumber VARCHAR(4),
    price VARCHAR(6)
);
INSERT INTO prestation (name, description, picturenumber, price) VALUES ("Decouvert" ,"Seance photo 20 min à Lyon et ses alentours","10", "40"),
("PRO" ,"Seance photo 40 min à Lyon et ses alentours","20", "70"),
("Mariage" ,"Phtographe mariage une journée à Lyon et ses alentours","90", "280");
ALTER TABLE prestation CHANGE name title VARCHAR(64);
ALTER TABLE prestation CHANGE photonumber picturenumber VARCHAR(10);

CREATE TABLE Contact(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (64),
    email VARCHAR (64),
    phoneNumber VARCHAR(14),
    message VARCHAR(540)
    );
INSERT INTO Contact (name, email, phoneNumber, message) VALUES
("Pierre Dubois" , "pierrr@gmail.com" , "0622546545", "Convectio coalito alioqui monstraret et praetorio monstraret praetorio causam tempestate praetorio compellebatur annonae convectio asperum."),
("Louis Paul" , "Lpaul@yahoo.fr" , "0633449568", "Convectio coalito alioqui monstraret et praetorio monstraret praetorio causam tempestate praetorio compellebatur annonae convectio asperum."),
("Lise Champ" , "champelise@yahoo.fr" ,"0655354685" ,"Convectio coalito alioqui monstraret et praetorio monstraret praetorio causam tempestate");

CREATE TABLE Admin (
    id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR (64),
    password VARCHAR (64)
);
INSERT INTO Admin (email, password) VALUES
("inno@gmail.com" , "test1234");
ALTER TABLE prestation CHANGE picturenumber photonumber INTEGER;
ALTER TABLE prestation CHANGE photonumber  pictureNumber INT;
ALTER TABLE prestation DROP COLUMN price ;
ALTER TABLE prestation ADD price INT;
UPDATE prestation SET price = 40 WHERE id= 1;
UPDATE prestation SET price = 80 WHERE id= 2;
UPDATE prestation SET price = 280 WHERE id= 3;
ALTER TABLE `Contact` CHANGE phoneNumber  phoneNumber INT;
ALTER TABLE `Contact` CHANGE phoneNumber  phoneNumber VARCHAR(12);
INSERT INTO image(id_album, image_name, date) VALUES (8, "portrait2.jpg", "2021-06-20"), 
(8, "portrait3.jpg", "2022-01-15"),
(8, "portrait4.jpg", "2022-05-15");
INSERT INTO image(id_album, image_name, date) VALUES
(8, "portrait5.jpg", "2022-06-13");

INSERT INTO image(id_album, image_name, date) VALUES (9, "mariage1.jpg", "2021-06-20"), 
(9, "mariage2.jpg", "2022-01-25"),
(9, "mariage3.jpg", "2022-02-11"),
(9, "mariage4.jpg", "2022-02-24"),
(9, "mariage5.jpg", "2022-05-17");
INSERT INTO prestation (title, description, picturenumber, price) VALUES ("Familiale" ,"Seance photo en famille de 4-6 personne à Lyon et ses alentours","30", "150"),
("Evenement" ,"Photographe pour un evenement toute une journée à Lyon et ses alentours","90", "170");

INSERT INTO prestation (title, description, picturenumber, price) VALUES
("photo Studio" ,"Seance photo 1 heure dans notre studio de photographie à Lyon","40", "90");