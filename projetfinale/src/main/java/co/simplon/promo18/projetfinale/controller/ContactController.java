package co.simplon.promo18.projetfinale.controller;

import java.util.List;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.CrossOrigin;
import co.simplon.promo18.projetfinale.Entity.Contact;
import co.simplon.promo18.projetfinale.Repository.ContactRepo;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/contact")
@RestController

public class ContactController {

    @Autowired
    ContactRepo contactRepo;

    // CREATE
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Contact save(@RequestBody Contact contact) {
        if (!contactRepo.save(contact)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return contact;
    }

    //READ
    @GetMapping()
    public List<Contact> findAll() {
        return contactRepo.findAll();
    }
    @GetMapping("/{id}")
    public Contact findById(@Min(1) @PathVariable int id) {
    Contact contact = contactRepo.findById(id);
    if (contact == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return contact;
    }
    
    //UPDATE
    @PutMapping
    public boolean updateById(@RequestBody Contact contact){
        return contactRepo.updateById(contact);
    }

    //DELETE
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@Min(1)@PathVariable int id) {; 
        if (!contactRepo.delete(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
}
