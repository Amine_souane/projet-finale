package co.simplon.promo18.projetfinale.controller;

import java.util.List;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.projetfinale.Entity.Admin;
import co.simplon.promo18.projetfinale.Repository.AdminRepo;

@RequestMapping("/api/admin")
@RestController
public class AdminController {
    
    @Autowired
    AdminRepo adminRepo;

    //CREATE
    @PostMapping()
    public Admin save(@RequestBody Admin admin ) {
        if (!adminRepo.save(admin)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return adminRepo.findById(admin.getId());
    }

    // READ
    @GetMapping()
    public List<Admin> findAll() {
            return adminRepo.findAll();
    }

    @GetMapping("/{id}")
    public Admin findById(@Min(1) @PathVariable int id) {
        Admin admin = adminRepo.findById(id);
        if (admin == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return admin;
    }

    //UPDATE
    @PutMapping
    public boolean updateById(@RequestBody Admin admin){
        return adminRepo.updateById(admin);
    }
    

    //DELETE
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@Min(1) @PathVariable int id) {
        if (!adminRepo.delete(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

}
