package co.simplon.promo18.projetfinale.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinale.Entity.Contact;

@Repository
public class ContactRepo {
    
    @Autowired
    private DataSource dataSource;

    //CREATE
     public boolean save(Contact contact) {
         try (Connection connection = dataSource.getConnection()) {
             PreparedStatement stmt = connection.prepareStatement(
               "INSERT INTO Contact (name, email, phoneNumber, message)" +
               "VALUES (?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS  
             );
             stmt.setString(1, contact.getName());
             stmt.setString(2, contact.getEmail());
             stmt.setString(3, contact.getPhoneNumber());
             stmt.setString(4, contact.getMessage());
             boolean done = stmt.executeUpdate() == 1;
             ResultSet rs = stmt.getGeneratedKeys();
             if (rs.next()) { 
                 contact.setId(rs.getInt(1));
             }
             return done;
            }catch (SQLException e) { 
                e.printStackTrace();
                throw new RuntimeException("Database access error");
            }
    }

    //READ
    public List<Contact> findAll() {
        List<Contact> contactList = new ArrayList<>();
        Contact newContact;

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement( 
                "SELECT * FROM Contact"
            );
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                newContact = new Contact(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("email"),
                    rs.getString("phoneNumber"),
                    rs.getString("message")
                );
                contactList.add(newContact);
            }
            return contactList;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    public Contact findById(int id) {
        Contact newContact = new Contact();

        try ( Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM Contact WHERE id= ?"
            );
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                newContact = new Contact(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("email"),
                    rs.getString("phoneNumber"),
                    rs.getString("message")
                );
                return newContact;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    //UPDATE

    public boolean updateById(Contact contact) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE Contact" 
            + " SET name = ? ,"
            + "email = ? ,"
            + "phoneNumber = ? ,"
            + "message = ? "
            + " WHERE id = ?"
            );
            stmt.setString(1, contact.getName());
            stmt.setString(2, contact.getEmail());
            stmt.setString(3, contact.getPhoneNumber());
            stmt.setString(4, contact.getMessage());
            stmt.setInt(5, contact.getId());

            return stmt.executeUpdate()==1;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        
    }
    //DELETE
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "DELETE FROM Contact WHERE id = ?"
            );
            stmt.setInt(1, id);
            boolean done = stmt.executeUpdate() == 1;
            System.out.println(done);
                return done;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

}
