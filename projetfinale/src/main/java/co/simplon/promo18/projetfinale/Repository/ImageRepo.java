package co.simplon.promo18.projetfinale.Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinale.Entity.Image;

@Repository
public class ImageRepo {
   
    @Autowired
    private DataSource dataSource;

    //CREATE
    public boolean save(Image image, int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "INSERT INTO image (image_name, date, id_album) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS
            );
            stmt.setString(1, image.getImage());
            stmt.setDate(2, Date.valueOf(image.getDate()));
            stmt.setInt(3, id);
            boolean done = stmt.executeUpdate() == 1;
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                image.setId(rs.getInt(1));
            }
            return done;
        }catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    //READ
    public List<Image> findAll() {
        List<Image> imageList = new ArrayList<>();
        Image newImage;

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM image"
            );
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                newImage = new Image(
                    rs.getInt("id"),
                    rs.getString("image_name"),
                    rs.getDate("date").toLocalDate()
                    
                );                
                imageList.add(newImage);

            }
            return imageList;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    public List<Image> findByAlbumId(int id) {
        List<Image> imageList = new ArrayList<>();
        Image newImage; 

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM image WHERE id_album = ?"   
            );
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                newImage = new Image(
                    rs.getInt("id"),                   
                    rs.getString("image_name"),
                    rs.getDate("date").toLocalDate()
                );
                imageList.add(newImage);
            }
            return imageList;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    public Image findById(int id) {
        Image newImage = new Image();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM image WHERE id= ?"
                );
                stmt.setInt(1, id);
                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    newImage = new Image(
                        rs.getInt("id"),
                        rs.getString("image_name"),
                        rs.getDate("date").toLocalDate()
                    );
                    return newImage;
                }
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    // UPDATE
    public boolean updateById(Image image) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE image"
            + " SET image_name = ?,"
            + "date = ?,"
            + " WHERE id = ?"
            );
            stmt.setString(1, image.getImage());
            stmt.setDate(2, Date.valueOf(image.getDate()));
            stmt.setInt(3, image.getId());

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        
    }


    //DELETE
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                " DELETE FROM image WHERE id = ?"
            );
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    
}
