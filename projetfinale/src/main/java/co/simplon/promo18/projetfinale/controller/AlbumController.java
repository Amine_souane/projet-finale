package co.simplon.promo18.projetfinale.controller;

import java.util.List;


import javax.validation.Valid;
import javax.validation.constraints.Min;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import co.simplon.promo18.projetfinale.UploadService;
import co.simplon.promo18.projetfinale.Entity.Album;
import co.simplon.promo18.projetfinale.Repository.AlbumRepo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/album")
@RestController

public class AlbumController {
    @Autowired
    AlbumRepo albumRepo;
    @Autowired
    UploadService uploadService;

    // CREATE
    @PostMapping
    public Album add(@ModelAttribute @Valid Album post,
            @ModelAttribute MultipartFile upload) {
        post.setId(null);

        post.setCover(uploadService.upload(upload));
        albumRepo.save(post);

        return post;
    }

    // READ
    @GetMapping()
    public List<Album> findAll() {
        return albumRepo.findAll();
    }

    @GetMapping("/{id}")
    public Album findById(@Min(1) @PathVariable int id) {
        Album album = albumRepo.findById(id);
        if (album == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return album;
    }

    @GetMapping("/image/{id}")
    public Album findByIdWithImageAlbum(@Min(1) @PathVariable int id) {
        Album album = albumRepo.findByIdWithImage(id);
        if (album == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return album;
    }

    // UPDATE
    @PutMapping
    public boolean updateById(@RequestBody Album album) {
        return albumRepo.updateById(album);
    }

    // DELETE
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@Min(1) @PathVariable int id) {
        ;
        if (!albumRepo.delete(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
}
