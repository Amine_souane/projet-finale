package co.simplon.promo18.projetfinale.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinale.Entity.Album;
import co.simplon.promo18.projetfinale.Entity.Image;


@Repository
public class AlbumRepo {

    @Autowired
	private DataSource dataSource;
    

    //CREATE
    public boolean save(Album album) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "INSERT INTO album (name, cover)" +
                    "VALUES (?, ?)", PreparedStatement.RETURN_GENERATED_KEYS
            );
        stmt.setString(1, album.getName());
        stmt.setString(2, album.getCover());
        boolean done = stmt.executeUpdate() == 1;
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            album.setId(rs.getInt(1));
        }
        return done;
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException("Database access error");
        }
    }
    //READ
    public List<Album> findAll() {
        List<Album> albumList = new ArrayList <>();
        Album newAlbum;

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM album"
            );
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                newAlbum = new Album(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("cover")
                    
                );
                albumList.add(newAlbum);
            }
            return albumList;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    public Album findById(int id) {
        Album newAlbum = null;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM album WHERE id= ?"
            );
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                newAlbum = new Album(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("cover")
                );
                return newAlbum;
            }
            return null;
        }catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");     
        
        }
    }

    public Album findByIdWithImage(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement
            ("SELECT * FROM image inner join album on album.id=image.id_album WHERE album.id=?");
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();
            List <Image> listImage = new ArrayList<Image>();
            while (rs.next()) {
                Album album = new Album(
                    rs.getInt("album.id"),
                    rs.getString("name"),
                    rs.getString("cover")
                );
                while (rs.next()){
                    Image image = new Image(
                        rs.getInt("image.id"),
                        rs.getString("image_name"),
                        rs.getDate("date").toLocalDate()
                    );
                    listImage.add(image);
                }
                album.setImage(listImage);
                return album;
            }
        } catch (SQLException e)  {

            e.printStackTrace();
        }

        return null;
    }
    
    //UPDATE
    public boolean updateById(Album album) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE album" 
            + " SET name = ? ,"
            + "cover = ? "
            + " WHERE id = ?"
            );
            stmt.setString(1, album.getName());
            stmt.setString(2, album.getCover());
            stmt.setInt(3, album.getId());
            

            return stmt.executeUpdate()==1;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        
    }
   
    //DELETE
    public boolean delete(int id) {
		try (Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					" DELETE FROM album WHERE id = ?"
			);
			stmt.setInt(1, id);
      boolean done = stmt.executeUpdate() == 1;
      System.out.println(done);
			return done;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database access error");
        }
    }
}



