package co.simplon.promo18.projetfinale.Entity;

import java.time.LocalDate;

public class Image {
    private Integer id;
    private String image;
    private LocalDate date;
	private Album album;
	
	public Image(Integer id, String image, LocalDate date) {
        this.id = id;
        this.image = image;
        this.date = date;
    }
    public Image() {
    }
    public Image(String image, LocalDate date, Album album) {
        this.image = image;
        this.date = date;
        this.album = album;
    }
    public Image(Integer id, String image, LocalDate date, Album album) {
		this.id = id;
		this.image = image;
		this.date = date;
		this.album = album;
	}
	public int getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}

	

}
