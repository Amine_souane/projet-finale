package co.simplon.promo18.projetfinale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetfinaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetfinaleApplication.class, args);
	}

}
