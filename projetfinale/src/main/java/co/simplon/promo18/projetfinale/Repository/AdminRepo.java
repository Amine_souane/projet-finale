package co.simplon.promo18.projetfinale.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinale.Entity.Admin;



@Repository
public class AdminRepo {
    @Autowired
    private DataSource dataSource;

    //CREATE 
    public boolean save(Admin admin) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "INSERT INTO Admin (email, password)" +
                    "VALUES (?, ?)", PreparedStatement.RETURN_GENERATED_KEYS 
            );
            stmt.setString(1, admin.getEmail());
            stmt.setString(2, admin.getPassword());
            boolean done = stmt.executeUpdate() == 1;
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                admin.setId(rs.getInt(1));
            }
            return done;
        } catch (SQLException e) { 
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    // READ
    public List<Admin> findAll() { 
        List<Admin> adminList = new ArrayList<>();
        Admin newAdmin;

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM Admin"
            );
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                newAdmin = new Admin(
                    rs.getString("email"),
                    rs.getString("password")
                );
                adminList.add(newAdmin);
            }
            return adminList;
        } catch (SQLException e) { 
         e.printStackTrace();
         throw new RuntimeException("database access error");         
        }
    }

    public Admin findById(int id) { 
        Admin newAdmin = new Admin();

        try (Connection connection = dataSource.getConnection()) { 
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM Admin WHERE id= ?"
            );
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) { 
                newAdmin = new Admin(
                    rs.getInt("id"),
                    rs.getString("email"),
                    rs.getString("password")
                );
                return newAdmin;
            }
            return null;
        } catch (SQLException e) { 
            e.printStackTrace();
            throw new RuntimeException("Databse access error");
        }
    }

    //UPDATE
    public boolean updateById(Admin admin) {
        try (Connection connection = dataSource.getConnection()) { 
            PreparedStatement stmt = connection.prepareStatement("UPDATE Admin"
            + " SET email = ?,"
            + "password = ?"
            + " WHERE id = ?"
            );
            stmt.setString(1, admin.getEmail());
            stmt.setString(2, admin.getPassword());
            stmt.setInt(3, admin.getId());

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }

    //DELETE
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                " DELETE FROM Admin WHERE id = ?"
            );
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) { 
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }



}

