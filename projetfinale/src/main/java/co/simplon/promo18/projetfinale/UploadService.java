package co.simplon.promo18.projetfinale;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UploadService {

    public String upload(MultipartFile upload) {
        String filename = UUID.randomUUID() + "." + FilenameUtils.getExtension(upload.getOriginalFilename());

        try {
            upload.transferTo(new File(getUploadFolder(), filename));

            return filename;
        } catch (IllegalStateException | IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "File upload failed");
        }
    }
    



    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/upload"));
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder;
    }
}
