package co.simplon.promo18.projetfinale.Entity;

import java.util.ArrayList;
import java.util.List;

public class Album {
    private Integer id;
    private String name;
    private String cover;
    private List<Image> image = new ArrayList<>();

    public Album(Integer id, String name, String cover) {
        this.id = id;
        this.name = name;
        this.cover = cover;
    }
    public Album() {
    }
    public Album(String name, String cover, List<Image> image) {
        this.name = name;
        this.cover = cover;
        this.image = image;
    }
    public Album(Integer id, String name, String cover, List<Image> image) {
        this.id = id;
        this.name = name;
        this.cover = cover;
        this.image = image;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCover() {
        return cover;
    }
    public void setCover(String cover) {
        this.cover = cover;
    }
    public List<Image> getImage() {
        return image;
    }
    public void setImage(List<Image> image) {
        this.image = image;
    }
}
    
    