package co.simplon.promo18.projetfinale.Entity;

public class Prestation {
    private Integer id;
    private String title;
    private String description;
    private Integer pictureNumber;
    private Integer price;
    public Prestation() {
    }
    public Prestation(String title, String description, Integer pictureNumber, Integer price) {
        this.title = title;
        this.description = description;
        this.pictureNumber = pictureNumber;
        this.price = price;
    }
    public Prestation(Integer id, String title, String description, Integer pictureNumber, Integer price) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.pictureNumber = pictureNumber;
        this.price = price;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getPictureNumber() {
        return pictureNumber;
    }
    public void setPictureNumber(Integer pictureNumber) {
        this.pictureNumber = pictureNumber;
    }
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }
}