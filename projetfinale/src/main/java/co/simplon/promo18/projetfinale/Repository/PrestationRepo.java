package co.simplon.promo18.projetfinale.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinale.Entity.Prestation;

@Repository
public class PrestationRepo {
    
    @Autowired
    private DataSource dataSource;

    //CREATE
    public boolean save(Prestation prestation) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "INSERT INTO prestation (title, description, pictureNumber, price)" +
                    "VALUES (?, ?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS
            );
            stmt.setString(1, prestation.getTitle());
            stmt.setString(2, prestation.getDescription());
            stmt.setInt(3, prestation.getPictureNumber());
            stmt.setInt(4, prestation.getPrice());
            boolean done = stmt.executeUpdate() == 1;
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                prestation.setId(rs.getInt(1));
            }
            return done;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

    }

    //READ
    public List<Prestation> findAll() {
        List<Prestation> prestationList = new ArrayList<>();
        Prestation newPrestation;

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM prestation"
            );
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                newPrestation = new Prestation(
                    rs.getInt("id"),
                    rs.getString("title"),
                    rs.getString("description"),
                    rs.getInt("pictureNumber"),
                    rs.getInt("price")
                );
                prestationList.add(newPrestation);
            }
            return prestationList;
        } catch (SQLException e) { 
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    public Prestation findById(int id) {
        Prestation newPrestation = null;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM prestation WHERE id= ?"
            );
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                newPrestation = new Prestation(
                    rs.getInt("id"),
                    rs.getString("title"),
                    rs.getString("description"),
                    rs.getInt("pictureNumber"),
                    rs.getInt("price")
                );
                return newPrestation;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
    //UPDATE
    public boolean updateById(Prestation prestation) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE prestation" 
            + " SET title = ? ,"
            + "description = ? ,"
            + "pictureNumber = ? ,"
            + "price = ? "
            + "WHERE id = ?"
            );
            stmt.setString(1, prestation.getTitle());
            stmt.setString(2, prestation.getDescription());
            stmt.setInt(3, prestation.getPictureNumber());
            stmt.setInt(4, prestation.getPrice());
            stmt.setInt(5, prestation.getId());
            

            return stmt.executeUpdate()==1;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
        
    }


    //DELETE
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "DELETE FROM prestation WHERE id = ?"
            );
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }
    }
}
