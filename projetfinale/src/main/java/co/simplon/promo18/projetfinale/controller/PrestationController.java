package co.simplon.promo18.projetfinale.controller;

import java.util.List;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.projetfinale.Entity.Prestation;
import co.simplon.promo18.projetfinale.Repository.PrestationRepo;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/prestation")
@RestController

public class PrestationController {
    
    @Autowired
    PrestationRepo prestationRepo;

    //CREATE
    @PostMapping()
    public Prestation save(@RequestBody Prestation prestation) {
        if (!prestationRepo.save(prestation)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return prestationRepo.findById(prestation.getId());
    }

    //READ
    @GetMapping()
    public List<Prestation> findAll() {
        return prestationRepo.findAll();
    }

    @GetMapping("/{id}")
    public Prestation findById(@Min(1) @PathVariable int id ) {
    Prestation prestation = prestationRepo.findById(id);
    if (prestation == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return prestation;
    }

    //UPDATE
    @PutMapping
    public boolean updateById(@RequestBody Prestation prestation){
        return prestationRepo.updateById(prestation);
    }


    //DELETE
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@Min(1)@PathVariable int id) {;
        if (!prestationRepo.delete(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
}
