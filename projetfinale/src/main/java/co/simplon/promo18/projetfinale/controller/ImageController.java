package co.simplon.promo18.projetfinale.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.projetfinale.UploadService;
import co.simplon.promo18.projetfinale.Entity.Image;
import co.simplon.promo18.projetfinale.Repository.ImageRepo;
import org.springframework.web.bind.annotation.PutMapping;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/image")
@RestController
public class ImageController {

    @Autowired
    ImageRepo imageRepo;
    @Autowired
    UploadService uploadService;

    // CREATE
    @PostMapping("/{id}")
    public Image add(@ModelAttribute @Valid Image post,
            @ModelAttribute MultipartFile upload, @PathVariable int id) {
        post.setDate(LocalDate.now());
        post.setId(null);

        post.setImage(uploadService.upload(upload));

        imageRepo.save(post, id);

        return post;
    }
    
    //READ
    @GetMapping()
    public List<Image> findAll() {
            return imageRepo.findAll();
    }

    @GetMapping("/{id}")
    public Image findById(@Min(1) @PathVariable int id) {
        Image image = imageRepo.findById(id);
        if (image == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return image;      
    }

    @GetMapping("/album/{id}")
    public List<Image> findbyAlbumId(@Min(1) @PathVariable int id) {
        return imageRepo.findByAlbumId(id);
    }

    // UPDATE
    @PutMapping
    public boolean updateById(@RequestBody Image image){
        return imageRepo.updateById(image);
    }
    // DELETE
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@Min(1) @PathVariable int id) {
        if (!imageRepo.delete(id)) throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

}
